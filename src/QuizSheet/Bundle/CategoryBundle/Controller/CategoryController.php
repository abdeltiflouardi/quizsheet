<?php

namespace QuizSheet\Bundle\CategoryBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class CategoryController extends FOSRestController
{
    /**
     * 
     * @Rest\View()
     */
    public function indexAction()
    {
        $c = $this->get('doctrine_mongodb')
                ->getRepository('QuizSheetCategoryBundle:Category')
                ->findAll();

        return $c;             
    }

    /**
     * @Rest\View()
     */
    public function cgetAction($name)
    {
        $c = $this->get('doctrine_mongodb')
                ->getRepository('QuizSheetCategoryBundle:Category')
                ->findByName(new \MongoRegex("/$name/i"));

        return $c;        
    }

}
