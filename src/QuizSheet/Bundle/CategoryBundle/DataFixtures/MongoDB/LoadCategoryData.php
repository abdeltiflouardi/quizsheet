<?php

namespace QuizSheet\Bundle\CategoryBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use QuizSheet\Bundle\CategoryBundle\Document\Category;

class LoadCategoryData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $java = new Category();
        $java->setName('JAVA');

        $php = new Category();
        $php->setName('PHP');

        $manager->persist($java);
        $manager->persist($php);
        $manager->flush();
    }
}